gsap.registerPlugin(ScrollTrigger, ScrollSmoother);


/* CREATE SMOOTHER */

const smoother = ScrollSmoother.create({
  wrapper: "#wrapper",
  content: "#content",
  smooth: 2,
  normalizeScroll: true, // prevents address bar from showing/hiding on most devices, solves various other browser inconsistencies
  ignoreMobileResize: true, // skips ScrollTrigger.refresh() on mobile resizes from address bar showing/hiding
  effects: true,
  preventDefault: true,
});


/* FADE IN PAGE */

gsap.to("#content", {
  duration: 1,
  //y: "-10rem",
  //scale: 1,
  opacity: 1,
  filter: "blur(0px)",
  delay: 0.5,
});


/* ANIMATE SECTIONS */

const sections = document.querySelectorAll(".projects .project");

sections.forEach(function (section) {
  gsap.from(section, {
    duration: 0.5,
    y: "5rem",
    opacity: 0,
    filter: "blur(10px)",
    scrollTrigger: {
      trigger: section,
      start: "top 90%", // "triggerElement page"
      //toggleActions: defaultActions,
    },
  });
});

const headers = document.querySelectorAll("h3");

headers.forEach(function (section) {
  gsap.from(section, {
    duration: 0.75,
    //x: "10rem",
    opacity: 0,
    filter: "blur(10px)",
    scrollTrigger: {
      trigger: section,
      start: "top 90%", // "triggerElement page"
      //toggleActions: defaultActions,
    },
  });
});


/* PIN CONTACT */

const contact = document.querySelector(".contact");

gsap.to(contact, {
  //height: "initial",
  //'min-height': "initial",
  //padding: "1rem",
  scrollTrigger: {
    trigger: contact,
    start: "top top",
    pin: true,
    pinSpacing: false,
    endTrigger: 'body', // the element that the scrolling should stop at
    end: 'bottom', // where the scrolling should stop
    toggleActions: 'play none none reverse', // what happens when the element goes off screen
  },
});

// gsap.to("#scroll-to-top", {
//   scrollTrigger: {
//     trigger: contact,
//     start: "top top",
//     pin: true,
//     pinSpacing: false,
//     endTrigger: 'body', // the element that the scrolling should stop at
//     end: 'bottom', // where the scrolling should stop
//     toggleActions: 'play none none reverse', // what happens when the element goes off screen
//   },
// });


/* SCROLL BUTTONS */

document.querySelector("#button-freelance-work").addEventListener("click", e => {
  // scroll to the spot where .box-c is in the center.
  // parameters: element, smooth, position
  smoother.scrollTo("#freelance-work", true, "center center");
  // or you could animate the scrollTop:
  // gsap.to(smoother, {
  // 	scrollTop: smoother.offset(".box-c", "center center"),
  // 	duration: 1
  // });
});

document.querySelector("#button-personal-projects").addEventListener("click", e => {
  // scroll to the spot where .box-c is in the center.
  // parameters: element, smooth, position
  smoother.scrollTo("#personal-projects", true, "center center");
  
  // or you could animate the scrollTop:
  // gsap.to(smoother, {
  // 	scrollTop: smoother.offset(".box-c", "center center"),
  // 	duration: 1
  // });
});

document.querySelector("#back-to-top").addEventListener("click", e => {
  // scroll to the spot where .box-c is in the center.
  // parameters: element, smooth, position
  smoother.scrollTo("h1", true, "center center");
  
  // or you could animate the scrollTop:
  // gsap.to(smoother, {
  // 	scrollTop: smoother.offset(".box-c", "center center"),
  // 	duration: 1
  // });
});

//ScrollTrigger.defaults({ scroller: smoother }); // this screws up click events
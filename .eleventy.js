const eleventySass = require("eleventy-sass");
const path = require("path");
require('dotenv').config();

module.exports = function (eleventyConfig) {
  
    eleventyConfig.addPassthroughCopy('src/assets/fonts');
    eleventyConfig.addPassthroughCopy('src/assets/js');
    eleventyConfig.addPassthroughCopy('src/assets/images/**/*.png');
    eleventyConfig.addPassthroughCopy('src/assets/images/**/*.svg');
    // eleventyConfig.addPassthroughCopy('src/assets/images/**/*.jpg');
    // eleventyConfig.addPassthroughCopy('src/assets/images/**/*.gif');
  

    const sassOptions = {
      compileOptions: {
        permalink: function(contents, inputPath) {
          return path.format({
            dir: "assets/styles",
            name: path.basename(inputPath, path.extname(inputPath)),
            ext: "." + process.env.VERSION + ".css"
          });
        }
      },
      // sass: {
      //   loadPaths: ["src/_includes"],
      //   style: "expanded",
      //   sourceMap: true,
      // },
      // defaultEleventyEnv: "development"
    };

    //plugins
    eleventyConfig.addPlugin(eleventySass, sassOptions);

    // eleventyConfig.addFilter('isActive', (arr) => arr.filter(guest => guest.name != ''));
    // eleventyConfig.addFilter('day', (arr,givenDate, givenLoc) => arr.filter(guest => guest.dates.some(e => (e.date == givenDate && e.day == givenLoc))));
    // eleventyConfig.addFilter('night', (arr,givenDate, givenLoc) => arr.filter(guest => guest.dates.some(e => (e.date == givenDate && e.night == givenLoc))));
    // //eleventyConfig.addFilter('total', (arr) => arr.reduce((prev,current) => ({...prev, count: current.count + prev.count})));
    // eleventyConfig.addFilter('total', arr => arr.map(a => a.count).reduce((prev,current) => current + prev));
    // //https://stackoverflow.com/questions/5732043/how-to-call-reduce-on-an-array-of-objects-to-sum-their-properties
    // eleventyConfig.addFilter('byGuest', (arr, givenSlug) => arr.filter(guest => guest.slug == givenSlug));
    // eleventyConfig.addFilter('byDate', (arr,givenDate) => arr.filter(d => d.date == givenDate));
    // eleventyConfig.addFilter('byAbbr', (arr,givenAbbr) => arr.filter(l => l.abbr == givenAbbr));

    return {
      dir: { input: "src", output: "_site", data: "_data" },
    };
  };